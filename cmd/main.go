package main

import (
	"github.com/docker/machine/libmachine/drivers/plugin"
	"salsa.debian.org/ina-guest/docker-machine-driver-qemu"
)

func main() {
	plugin.RegisterDriver(qemu.NewDriver("default", "path"))
}
